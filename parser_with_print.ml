open ASD
open Token

(* p? *)
let opt p = parser
  | [< x = p >] -> Some x
  | [<>] -> print_endline "NOPE"; None

(* p* *)
let rec many p = parser
  | [< x = p; l = many p >] -> x :: l
  | [<>] -> []

(* p+ *)
let some p = parser
  | [< x = p; l = many p >] -> x :: l

(* p (sep p)* *)
let rec list1 p sep = parser
  | [< x = p; l = list1_aux p sep >] -> x :: l
and list1_aux p sep = parser
  | [< _ = sep; l = list1 p sep >] -> l
  | [<>] -> []

(* (p (sep p)* )? *)
let list0 p sep = parser
  | [< l = list1 p sep >] -> l
  | [<>] -> print_endline "NOPE"; []


(* TODO : change when you extend the language
let rec program = parser
  | [< e = function_declaration; _ = Stream.empty ?? "unexpected input at the end" >] -> e
*)

let rec program = parser
  (*| [< f = some function_declaration >] -> print_endline "End of parsing\n\n"; f*)
  | [< exp = expression >] -> exp

and function_declaration = parser
  | [< f = function_prototype >] -> print_endline "PROTO"; f
  | [< f = function_definition >] -> print_endline "FUN"; f

and function_prototype = parser
  | [< 'PROTO_KW; t = function_type; 'IDENT n; 'LP; p = list0 function_parameter comma; 'RP; >] -> print_endline n; Function_prototype (t,Identifier (n,Variable) ,p)

and function_definition = parser
  | [< 'FUNC_KW; t = function_type; 'IDENT n; 'LP; p = list0 function_parameter comma; 'RP; i = instruction_parser >] -> print_endline ("Function "^n); Function_definition (t,Identifier (n,Variable),p,i)

and function_parameter = parser
  | [< 'IDENT p; a = function_parameter_array >] -> print_endline ("Parameter "^p); Parameter (Identifier (p,a))

and function_parameter_array = parser
  | [< 'LB; 'RB>] -> print_endline "An array"; Array (IndexNone)
  | [< >] -> print_endline "Not array"; Variable


and function_type = parser
  | [< 'VOID_KW >] -> print_endline "VOID type"; TypeVoid
  | [< 'INT_KW >] -> print_endline "INT type"; TypeInt



and instruction_parser = parser
  | [< i = instruction_block >] -> print_endline "Instruction"; i
  | [< i = return_instruction >] -> print_endline "Instruction"; i
  | [< i = print_instruction >] -> print_endline "Instruction"; i
  | [< i = read_instruction >] -> print_endline "Instruction"; i
  | [< i = if_instruction >] -> print_endline "Instruction"; i
  | [< i = while_instruction >] -> print_endline "Instruction"; i
  | [< 'IDENT base; exp = instruction_litteral base >] -> print_endline "Instruction beginning with an ident"; exp

and instruction_litteral base = parser
  | [< 'LP; p = list0 expression comma; 'RP >] -> print_endline "Function call parsed"; FunctionCall (Identifier(base, Variable), p)
  | [< a = array_var; 'ASSIGN; e = expression >] -> print_endline "Assigment instruction"; Assign ( Identifier(base, a), e)

and instruction_block = parser
  | [< 'LC; declarations = many declaration; instructions = some instruction_parser; 'RC >] -> print_endline "Instruction block parsed"; Block (declarations,instructions)

and declaration = parser
  | [< f = function_declaration >] -> print_endline "Function declaration"; FunDeclar f
  | [< 'INT_KW; v = list1 variable comma >] -> print_endline "Variable declaration"; VarDeclar v

and return_instruction = parser
  | [< 'RETURN_KW; exp = expression >] -> print_endline "Return parsed"; Return exp

and print_instruction = parser
  | [< 'PRINT_KW; items = list1 item_parser comma >] -> print_endline "Print parsed"; Print items

and read_instruction = parser
  | [< 'READ_KW; items = list1 item_parser comma >] -> print_endline "Read parsed"; Read items

and item_parser = parser
  | [< exp = expression >] -> print_endline "The item is an expression"; ItemExpr exp
  | [< 'TEXT str >] -> print_endline "The item is a string"; ItemString str

and if_instruction = parser
  | [< 'IF_KW; exp = expression; 'THEN_KW; inst_if = instruction_parser; inst_else = else_statement; 'FI_KW>] -> print_endline "If parsed"; If (exp, inst_if, inst_else)


and else_statement = parser
  | [< 'ELSE_KW; inst = instruction_parser >] -> print_endline "Else statement parsed"; ElseInstruction inst
  | [< >] -> print_endline "No else statement"; NoElse

and while_instruction = parser
  | [< 'WHILE_KW; exp = expression; 'DO_KW; inst = instruction_parser; 'DONE_KW >] -> print_endline "While parsed"; While (exp, inst)




and expression = parser
  | [< e1 = factor; e = expression_aux e1 >] -> print_endline "Parsed expression"; e

and expression_aux e1 = parser
  | [< 'PLUS;  e2 = factor; e = expression_aux e2 >] -> print_endline "Parsed expression_aux PLUS"; ExpBin (e1, PlusOp, e)
  | [< 'MINUS;  e2 = factor; e = expression_aux e2 >] -> print_endline "Parsed expression_aux MINUS"; ExpBin (e1, MinusOp, e)
  | [<>] -> print_endline "Parsed expression_aux empty"; e1

and factor = parser
  | [< e1 = primary; e = factor_aux e1 >] -> print_endline "Parsed factor"; e

and factor_aux e1 = parser
  | [< 'MUL; e = primary; e2 = factor_aux e>] -> print_endline "Parsed factor_aux MUL"; ExpBin (e1, MulOp, e2)
  | [< 'DIV; e = primary; e2 = factor_aux e>] -> print_endline "Parsed factor_aux DIV"; ExpBin (e1, DivOp, e2)
  | [<>] -> print_endline "Parsed factor_aux empty"; e1

and primary = parser

  | [< 'INTEGER x >] -> print_endline "Parsed primary int"; ExpInt x
  | [< 'IDENT base; l = litteral base >] -> print_endline ("Variable parsed = "^base); l
  | [< 'LP; x = expression; 'RP>] -> print_endline "Parsed parenthesis"; x

and litteral base = parser
  | [< 'LP; e = list0 expression comma; 'RP >] -> print_endline ("Calling "); ExpCall (Identifier (base, Variable), e)
  | [< a = array_var >] -> print_endline ("Variable "); ExpIdent (Identifier (base, a))


and array_var = parser
| [< 'LB; exp = expression; 'RB>] -> print_endline "An array"; Array (IndexExp exp)
| [< >] -> print_endline "Not array"; Variable

and variable = parser
  | [< 'IDENT n; a = array_var >] -> print_endline ("Variable "^n); Identifier (n,a)



and comma = parser
  | [< 'COM >] -> ()
