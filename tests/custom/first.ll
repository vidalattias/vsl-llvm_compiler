; Target
target triple = "x86_64-apple-macosx10.14.0"
; External declaration of the printf function
declare i32 @scanf(i8* noalias nocapture, ... )
declare i32 @printf(i8* noalias nocapture, ...)

; Actual code begins

define i32 @main()
{
	%tmp2 = load i32, i32* %vidal
	store i32 2, i32* %test
	ret i32 0
}

