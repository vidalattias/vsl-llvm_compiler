; Target
target triple = "x86_64-apple-macosx10.14.0"
; External declaration of the printf function
declare i32 @printf(i8* noalias nocapture, ...)

; Actual code begins

define void @main()
{
	call void @compare(i32 2, i32 1)
	call void @compare(i32 1, i32 2)
	call void @compare(i32 1, i32 1)
	ret void
}
@.fmt5 = global [18 x i8] c"%d est egal a %d\0A\00"
@.fmt4 = global [24 x i8] c"%d est different de %d\0A\00"
define void @compare(i32 %paramx, i32 %paramy)
{
	%x = alloca i32
	store i32 %paramx, i32* %x
	%y = alloca i32
	store i32 %paramy, i32* %y
	%tmp2 = load i32, i32* %y
	%tmp1 = load i32, i32* %x
	%tmp3 = sub i32 %tmp1, %tmp2
	%tmp4 = icmp ne i32 %tmp3, 0
	br i1 %tmp4, label %then1, label %else2

then1:
	%tmp5 = load i32, i32* %x
	%tmp6 = load i32, i32* %y
	call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8 ]* @.fmt4 , i64 0, i64 0) , i32 %tmp5 , i32 %tmp6)
	br label %fi3

else2:
	%tmp7 = load i32, i32* %x
	%tmp8 = load i32, i32* %y
	call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8 ]* @.fmt5 , i64 0, i64 0) , i32 %tmp7 , i32 %tmp8)
	br label %fi3

fi3:
	ret void
}

