open ASD

let rec print_tab n =
  match n with
  | 0 -> ""
  | _ -> "  "^("\n")^print_tab (n-1)

(* main function. return only a string *)
let rec prettyprint ast =
  match ast with
  | h::t -> (p_function_declaration h)^("\n")^(prettyprint t)
  | _ -> ""

and p_function_declaration ast =
  match ast with
  | Function_definition (t,id,p,ins) -> ("Function definition ")^("\n")^(p_vsl_type t)^("\n")^(p_identifier id)^("\n")^(p_param_list p)^("\n")^(p_instruction ins)
  | Function_prototype (t,id,p) -> ("Function prototype ")^("\n")^(p_vsl_type t)^("\n")^(p_identifier id)^("\n")^(p_param_list p)

and p_param_list ast =
  match ast with
  | h::t -> (p_parameter h)^("\n")^(p_param_list t)
  | _ -> ""

and p_parameter ast =
  match ast with
  | Parameter (id) -> p_identifier id

and p_vsl_type ast =
  match ast with
  | TypeVoid -> "VOID "
  | TypeInt -> "INT "

and p_inst_list ast =
  match ast with
  | h::t -> (p_instruction h)^("\n")^(p_inst_list t)
  | _ -> ""


and p_instruction ast =
  match ast with
  | Block (d, ins) -> (p_declar_list d)^("\n")^(p_inst_list ins)
  | Assign (id,exp) -> (p_identifier id)^("\n")^(p_expression exp)
  | Return (exp) -> (p_expression exp)
  | Print (item) -> (p_item_list item)
  | Read (item) -> (p_item_list item)
  | If (exp, ins, else_i) -> (p_expression exp)^("\n")^(p_instruction ins)^("\n")^(p_else_statement else_i)
  | While (exp, ins) -> (p_expression exp)^("\n")^(p_instruction ins)
  | FunctionCall (id, exp) -> (p_identifier id)^("\n")^(p_exp_list exp)

and p_declar_list ast =
  match ast with
  | h::t -> (p_declaration h)^("\n")^(p_declar_list t)
  | _ -> ""

and p_declaration ast =
  match ast with
  | VarDeclar (id) -> (p_id_list id)
  | FunDeclar (f) -> (p_function_declaration f)

and p_item_list ast =
  match ast with
  | h::t -> (p_item h)^("\n")^(p_item_list t)
  | _ -> ""

and p_item ast =
  match ast with
  | ItemExpr (exp) -> (p_expression exp)
  | ItemString (str) -> (str)

and p_else_statement ast =
  match ast with
  | ElseInstruction (ins) -> (p_instruction ins)
  | NoElse -> ("There is no else statement")

and p_exp_list ast =
  match ast with
  | h::t -> (p_expression h)^("\n")^(p_exp_list t)
  | _ -> ""

and p_expression ast =
  match ast with
  | ExpInt (number) -> (string_of_int number)
  | ExpIdent (id) -> (p_identifier id)
  | ExpBin (exp1, op, exp2) -> (p_expression exp1)^("\n")^(p_bin_operator op)^("\n")^(p_expression exp2)
  | ExpCall (id, exp) -> (p_identifier id)^("\n")^(p_exp_list exp)

  and p_bin_operator ast =
    match ast with
    | PlusOp -> ("PLUS")
    | MinusOp -> ("MINUS")
    | MulOp -> ("MUL")
    | DivOp -> ("DIV")

and p_id_list ast =
  match ast with
  | h::t -> (p_identifier h)^("\n")^(p_id_list t)
  | _ -> ""

and p_identifier ast =
  match ast with
  | Identifier (str, arr) -> (str)^("\n")^(p_array arr)

and p_array ast =
  match ast with
  | Variable -> ("Not an array")
  | Array (index) -> (p_array_index index)

and p_array_index ast =
  match ast with
  | IndexInt (number) -> (string_of_int number)
  | IndexExp (exp) -> (p_expression exp)
  | IndexNone -> ("Array declaration")
