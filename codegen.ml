open ASD
open Llvm
open Utils
open List
open SymbolTable


type print_items_pair =
{
  code_item: llvm_instr list;
  place_item: string list;
}

(* main function. return only a string: the generated code *)
let empty_ir = {header=[];code=[]}

let rec ir_of_ast p =
  (* this header describe to LLVM the target
   * and declare the external function printf
   *)
  let head = "; Target\n" ^
  "target triple = \"x86_64-apple-macosx10.14.0\"\n" ^
  "; External declaration of the printf function\n" ^
  "declare i32 @scanf(i8* noalias nocapture, ... )\n" ^
  "declare i32 @printf(i8* noalias nocapture, ...)\n" ^
  "\n; Actual code begins\n\n"

  (* TODO : change when you extend the language *)
  in let ir = ir_of_program p
  (* adds the return instruction *)
  in let new_ir = {
    header = ir.header;
    code = LLVM_Return {
      ret_type = LLVM_Type_Int;
      ret_value = "coucou";
    } :: ir.code
  }

  (* generates the final string *)
  in head ^

  (* We create the function main *)
  (string_of_ir ir)

(* All main code generation functions take the current IR and a scope,
 * append header and/or code to the IR, and/or change the scope
 * They return the new pair (ir, scope)
 * This is convenient with List.fold_left
 *
 * They can return other stuff (synthesized attributes)
 * They can take extra arguments (inherited attributes)
 *)


(* returns the regular pair, plus the pair of the name of the result and its type *)

and ir_of_program =
  let combine ast ir_tail =
      match ast with
        | Function_definition (typ, id, param, inst) ->
                let ir_fun = ir_of_fun_declaration (FunDeclar ast)

                in let ir_return = {
                    header = ir_fun.header @ ir_tail.header;
                    code = ir_tail.code @ ir_fun.code;
                }

                in ir_return
      | Function_prototype (typ, id, param) -> ir_tail
  in function
  | h::t -> combine h (ir_of_program t)
  | [] -> empty_ir


and ir_of_block ast =
  match ast with
  | Block (declare_list,instr_list) ->
    let ir_declare = ir_of_declaration_list declare_list
    in let ir_instr = ir_of_instruction_list instr_list
    in {
        header =  ir_declare.header @ ir_instr.header;
        code =  ir_instr.code @ ir_declare.code
      }


and ir_of_while ast =
  match ast with
  | While (exp, instructions) ->
    let ir_exp, (result_name, result_type) = ir_of_expression exp
    in let icmp_temp = newtmp ()
    in let lbl_while = newlab "while"
    in let lbl_do = newlab "do"
    in let lbl_done = newlab "done"
    in let ir_do = ir_of_instruction_list [instructions]
    in let code =
    LLVM_While {
      exp_code = ir_exp;
      exp_tmp = result_name;
      icmp_tmp = icmp_temp;
      label_while = lbl_while;
      label_do = lbl_do;
      label_done = lbl_done;
      ir_do = ir_do;
    }
    in
    {
      header = [];
      code = [code]
    }



and ir_of_conditionnal ast =
  match ast with
  | If (exp, ast_then, ast_else) ->
    let ir_exp, (result_name, result_type) = ir_of_expression exp
    in let icmp_temp = newtmp ()
    in let lbl_then = newlab "then"
    in let lbl_else = newlab "else"
    in let lbl_fi = newlab "fi"
    in let ir_then = ir_of_instruction_list [ast_then]
    in let ir_else = ir_of_else ast_else
    in let code =
    LLVM_Conditional {
      exp_tmp = result_name;
      icmp_tmp = icmp_temp;
      label_then = lbl_then;
      label_else = lbl_else;
      ir_then = ir_then;
      ir_else = ir_else;
      label_fi = lbl_fi;
    }
    in
    {
      header = ir_then.header @ ir_else.header;
      code = code :: ir_exp.code;
    }

  and ir_of_else ast =
    match ast with
    | ElseInstruction ins -> ir_of_instruction_list [ins]
    | NoElse -> empty_ir



and ir_of_return ast =
match ast with
| Return (exp) ->
  let ir_exp, (exp_name, exp_type) = ir_of_expression exp

  in let code = LLVM_Return {
    ret_type = LLVM_Type_Int;
    ret_value = exp_name
  }
  in {
        header = [];
        code = code :: ir_exp.code;
      }


and ir_of_funcall =
  let rec combine_exp explist =
    match explist with
    | h::t ->
      let ir_head, (name_head, _) = (ir_of_expression h)
      in let (ir_tail, names) = (combine_exp t)
      in ({
        header = [];
        code = ir_head.code @ ir_tail.code
        },[name_head] @ names)
    | [] -> (empty_ir, [])
  in function
  | FunctionCall (id, explist) ->
    let ir_exp, names = (combine_exp explist)
    in let code = LLVM_Fun_call
    {
      call_name = string_of_identifier id;
      call_params = names
    }
    in {
          header = [];
          code = [code] @ ir_exp.code
       }



and ir_of_print =
  let item_to_string item =
    match item with
    | ItemExpr _ -> "%d"
    | ItemString str -> str
  in let rec item_list_to_string ast =
    match ast with
      | h::t -> (item_to_string h) ^ (item_list_to_string t)
      | [] -> ""

  in let rec item_list_to_ir ast =
    match ast with
      | (ItemExpr e)::t ->
        let label = newtmp


        in let ir_exp, (exp_name, exp_type) = ir_of_expression e

        in let pair_tail = item_list_to_ir t

        in {
              code_item = pair_tail.code_item @ ir_exp.code;
              place_item = exp_name :: pair_tail.place_item;
           }
      | (ItemString e)::t -> item_list_to_ir t
      | [] ->  { code_item = []; place_item = []}
  in function
  | Print (items) ->
    let content = item_list_to_string items
    in let items_list = item_list_to_ir items

    in let (new_string, length) = string_transform content
    in let str_ref = newlab "@.fmt"
    in let code_header = LLVM_Print_head
    {
      print_head_name = str_ref;
      print_head_content = new_string;
      print_head_content_len = string_of_int length
    }

    in let code = LLVM_Print
    {
      print_str_ref = str_ref;
      print_exp_list = items_list.place_item;
      print_content_len = string_of_int length
    }

    in
    {
        header = [code_header];
        code = code :: items_list.code_item
    }



and ir_of_read =
  let item_to_string item =
    match item with
    | ItemExpr _ -> "%d"
    | ItemString str -> str
  in let rec item_list_to_string ast =
    match ast with
      | h::t -> (item_to_string h) ^ (item_list_to_string t)
      | [] -> ""

  in let string_of_item_id id =
    match id with
    | ExpIdent id -> "* %" ^ (string_of_identifier id)
    | _ -> ""
  in let rec item_list_to_ir ast =
    match ast with
      | (ItemExpr e)::t -> string_of_item_id e :: item_list_to_ir t
      | (ItemString e)::t -> item_list_to_ir t
      | [] ->  []
  in function
  | Read (items) ->
    let content = item_list_to_string items
    in let items_list = item_list_to_ir items

    in let (new_string, length) = string_transform content
    in let str_ref = newlab "@.fmt"
    in let code_header = LLVM_Print_head
    {
      print_head_name = str_ref;
      print_head_content = new_string;
      print_head_content_len = string_of_int length
    }

    in let code = LLVM_Read
    {
      read_str_ref = str_ref;
      read_exp_list = items_list;
      read_content_len = string_of_int length
    }

    in
    {
        header = [code_header];
        code = [code]
    }



and ir_of_instruction_list =
  let select_instruction ast =
      match ast with
      | Assign (_,_) ->
        let ir_assig, _ = ir_of_assignment ast
        in ir_assig
      | Block _ -> ir_of_block ast
      | If _ -> ir_of_conditionnal ast
      | While _ -> ir_of_while ast
      | Return _ -> ir_of_return ast
      | FunctionCall _ -> ir_of_funcall ast
      | Print _ -> ir_of_print ast
      | Read _ -> ir_of_read ast
  in function
    | h::t ->
      let ir_ins = select_instruction h

      in let ir_tail = ir_of_instruction_list t
      in
      {
        header =  ir_ins.header @ ir_tail.header;
        code = ir_tail.code @ ir_ins.code
      }
    | [] -> empty_ir

and ir_of_declaration_list ast =
  let combine ir_head ir_tail =
    {
      header = ir_head.header @ ir_tail.header ;
      code = ir_head.code @ ir_tail.code
    }
  in match ast with
  | h::t -> combine (ir_of_declaration h) (ir_of_declaration_list t)
  | [] -> empty_ir


and ir_of_declaration ast =
  match ast with
  | VarDeclar _ -> ir_of_var_declaration ast
  | FunDeclar _ ->
  let ir_bis = ir_of_fun_declaration ast
  in {
        header = ir_bis.code;
        code= []
      }



and ir_of_var_declaration =
  let rec combine =
      let code id =
        LLVM_Declaration_variable {
          declare_name = "%" ^ (string_of_identifier id);
          declare_type = llvm_type_of_id id
      }
      in function
      | h::t -> code h::(combine t)
      | [] -> []
  in function
  | VarDeclar id ->
              {
                header = [];
                code = combine id
              }



and ir_of_fun_declaration =
  let param_to_string p =
    match p with
    | Parameter p -> string_of_identifier p

  in let rec param_list_to_string_list lst =
    match lst with
    | h::t -> (param_to_string h) :: (param_list_to_string_list t)
    | [] -> []

  in let aux ast =
  match ast with
  | Function_definition (typ, id, param, ins) ->
    let ir_body = ir_of_instruction_list [ins]
    in let code id =
      LLVM_Fun_def {
        fun_type = vsl_type_to_llvm_type typ;
        fun_name = string_of_identifier id;
        fun_body = ir_body;
        fun_params = param_list_to_string_list param;
      }

    in
      {
        header = [];
        code = [code id]
      }

  in function
  | FunDeclar f -> aux f





and ir_of_assignment ast =
  match ast with
  | Assign (id, exp) ->

    let result = newtmp()

    in let ir_exp, (exp_name, exp_type) = ir_of_expression exp

    in let ir_id, infos = ir_of_identifier id

    in let code = LLVM_Assign {
      value_name = "%" ^ (string_of_identifier id);
      value_type = LLVM_Type_Int;
      exp_name = exp_name;
      exp_type = exp_type;
    }
    in (
      {
        header = ir_exp.header;
        code = code :: ir_id.code @ ir_exp.code;
      }, (result, LLVM_Type_Int))

and ir_of_identifier ast =
  match ast with
  | Identifier (v, Variable) ->
    let result = newtmp ()
    in let code = LLVM_Identifier {
      value_name = result;
      value_type = LLVM_Type_Int;
      identifier_name = "%" ^ v;
    }

    in (
        {
          header = [];
          code = [code];
        }, (result, LLVM_Type_Int))
    | Identifier (v, Array (IndexExp exp)) ->

    let ir_exp, (exp_name, exp_type) = ir_of_expression exp

    in let code = LLVM_Ident_store {
      id_store_name = string_of_id v;
      id_store_place = exp_name;
    }

    in
    ({
      header = [];
      code = ir_exp.code;
      }, (exp_name, LLVM_Type_Array 0))

    | Identifier (v, Array (IndexInt number)) ->
    ({
      header = [];
      code = [];
      }, (string_of_int number, LLVM_Type_Array 0))

and ir_of_exp_call =
  let rec combine_exp explist =
    match explist with
    | h::t ->
      let ir_head, (name_head, _) = (ir_of_expression h)
      in let (ir_tail, names) = (combine_exp t)
      in ({
        header = [];
        code = ir_head.code @ ir_tail.code
        },[name_head] @ names)
    | [] -> (empty_ir, [])
  in function
  | (id, explist) ->
    let ir_exp, names = (combine_exp explist)
    in let return_place = newtmp()
    in let code = LLVM_Exp_call
    {
      call_name = string_of_identifier id;
      call_params = names;
      call_place = return_place
    }
    in ({
          header = [];
          code = [code] @ ir_exp.code
       }, (return_place, LLVM_Type_Int))


and ir_of_expression =
  (* function to generate all binop operations *)
  let string_of_op op =
    match op with
    | PlusOp -> "add"
    | MinusOp -> "sub"
    | MulOp -> "mul"
    | DivOp -> "udiv"
  in let aux op typ (l, r) =
    (* generation of left operand computation. We give directly (ir, scope) *)
    let ll, (lresult_name, lresult_type) = ir_of_expression l
    (* generation of right operand computation. We give directly (ir, scope) from the left computation *)
    (* it appends code to the previous code generated *)
    in let rr, (rresult_name, rresult_type) = ir_of_expression r

    (* allocate a new unique locale identifier *)
    and result = newtmp ()

    (* type checking *)
    in let _ = if lresult_type <> rresult_type || typ <> rresult_type then failwith "Type error"

    (* new instruction *)
    in let code = Binop {
      lvalue_name = result;
      lvalue_type = typ;
      op = string_of_op op;
      left = lresult_name;
      right = rresult_name;
    }

    (* Returns : *)
    in (
        {
          header = ll.header @ rr.header;
          code = code :: ll.code @ rr.code;
        }, (result, typ))

  in function
    | ExpBin (l, op, r)  -> aux op LLVM_Type_Int (l, r) (* For now, all binop are integer *)
    | ExpInt i    -> (empty_ir, (string_of_int i, LLVM_Type_Int))
    | ExpIdent id -> ir_of_identifier id
    | ExpCall (id, exp) -> ir_of_exp_call (id, exp)


and vsl_type_to_llvm_type ast =
  match ast with
  | TypeVoid -> LLVM_Type_Void
  | TypeInt -> LLVM_Type_Int

  and llvm_type_of_id id =
    match id with
    | Identifier (name, Array (IndexInt size)) -> LLVM_Type_Array size
    | Identifier (name, test) -> LLVM_Type_Array 0
