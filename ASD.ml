open Llvm

type code = llvm_instr list

type program = function_declaration list

and function_declaration =
  | Function_definition of vsl_type * identifier * parameter list * instruction
  | Function_prototype of vsl_type * identifier * parameter list

and parameter = Parameter of identifier

and param_type = VarId | ArrayId


and vsl_type =
  | TypeVoid
  | TypeInt

and instruction =
  | Block of declaration list * instruction list
  | Assign of identifier * expression
  | Return of expression
  | Print of item list
  | Read of item list
  | If of  expression * instruction * else_statement
  | While of expression * instruction
  | FunctionCall of identifier * expression list


and declaration =
  | VarDeclar of identifier list
  | FunDeclar of function_declaration

and item =
  | ItemExpr of expression
  | ItemString of string

and else_statement =
  | ElseInstruction of instruction
  | NoElse

and expression =
  | ExpInt of int
  | ExpIdent of identifier
  | ExpBin of expression * bin_operator * expression
  | ExpCall of identifier * expression list

and bin_operator =
  | PlusOp
  | MinusOp
  | MulOp
  | DivOp

and identifier = Identifier of string * array
and array = Variable | Array of array_index
and array_index =
  | IndexInt of int
  | IndexExp of expression
  | IndexNone

let string_of_identifier iden =
  match iden with
  | Identifier (id, _) -> id
