{
  open Lexing
  open Token
  open Printf

  type error = {
    character: char;
    line: int;
    pos: int;
  }

  exception Unexpected_character of error
}

(**********************************************************)

let caps = ['A'-'Z']
let letter = ['a'-'z']
let digit  = ['0'-'9']
let ascii  = _ # ['\n' '"']
let blanks = [' ' '\n' '\t']

rule tokenize = parse
  (* skip new lines and update line count (useful for error location) *)
  | '\n'
      { let _ = new_line lexbuf in tokenize lexbuf }

  (* skip other blanks *)
  | blanks
      { tokenize lexbuf }

  (* skip comments *)
  | "//" (_ # '\n')*
      { tokenize lexbuf }

  (* characters *)
  | '('
      { print_endline "Left parenthesis"; LP        :: tokenize lexbuf }
  | ';'
      { print_endline "Semicolo"; SC        :: tokenize lexbuf }

  | ')'
      { print_endline "Right parenthesis";RP        :: tokenize lexbuf }
  | '['
      { print_endline "Left bracket"; LB        :: tokenize lexbuf }
  | ']'
      { print_endline "Right bracket"; RB        :: tokenize lexbuf }
  | '{'
      { print_endline "Left curly bracket"; LC        :: tokenize lexbuf }
  | '}'
      { print_endline "Right curly bracket";RC        :: tokenize lexbuf }
  | '+'
      { print_endline "Plus";PLUS      :: tokenize lexbuf }
  | '-'
      { print_endline "Minus";MINUS      :: tokenize lexbuf }
  | '*'
      { print_endline "Multiplication";MUL      :: tokenize lexbuf }
  | '/'
      { print_endline "Division";DIV      :: tokenize lexbuf }
  | ','
      { print_endline "Comma";COM      :: tokenize lexbuf }

  | ":="
      { print_endline "Assign"; ASSIGN     :: tokenize lexbuf }

  | "WHILE"
      { print_endline "While"; WHILE_KW     :: tokenize lexbuf }
  | "DO"
      { print_endline "DO"; DO_KW     :: tokenize lexbuf }
  | "DONE"
      { print_endline "Done"; DONE_KW     :: tokenize lexbuf }
  | "IF"
      { print_endline "IF"; IF_KW     :: tokenize lexbuf }
  | "THEN"
      { print_endline "Then"; THEN_KW     :: tokenize lexbuf }
  | "ELSE"
      { print_endline "Else"; ELSE_KW     :: tokenize lexbuf }
  | "FI"
      { print_endline "Fi"; FI_KW     :: tokenize lexbuf }
  | "RETURN"
      { print_endline "Return"; RETURN_KW     :: tokenize lexbuf }
  | "PRINT"
      { print_endline "Print"; PRINT_KW     :: tokenize lexbuf }
  | "READ"
      { print_endline "Read"; READ_KW     :: tokenize lexbuf }
  | "FUNC"
      { print_endline "Function declaration";FUNC_KW      :: tokenize lexbuf }
  | "PROTO"
      { print_endline "Prototype";  PROTO_KW      :: tokenize lexbuf }
  | "VOID"
      { print_endline "Void return"; VOID_KW      :: tokenize lexbuf }
  | "INT"
      { print_endline "Int return"; INT_KW      :: tokenize lexbuf }

  (* TODO : other keywords *)

  (* other tokens (no conflict with keywords in VSL) *)
  | letter (letter | digit)* as lxm
      { printf "Id "; print_endline lxm; IDENT lxm :: tokenize lexbuf }


  | '"' (ascii* as lxm) '"'
      { printf "String \""; print_string lxm; print_endline "\""; TEXT lxm  :: tokenize lexbuf }


  | (digit+) as lxm
      { printf "Number "; print_endline lxm; INTEGER (int_of_string lxm) :: tokenize lexbuf }

  | eof
      { print_endline "\n\n"; [] }

  (* catch errors *)
  | _ as c
    {
      let e = {
          character = c;
          line = lexbuf.lex_curr_p.pos_lnum;
          pos  = lexbuf.lex_curr_p.pos_cnum - lexbuf.lex_curr_p.pos_bol;
        }
      in raise (Unexpected_character e)
    }
