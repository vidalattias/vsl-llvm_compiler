open ASD
open Token

(* p? *)
let opt p = parser
  | [< x = p >] -> Some x
  | [<>] -> None

(* p* *)
let rec many p = parser
  | [< x = p; l = many p >] -> x :: l
  | [<>] -> []

(* p+ *)
let some p = parser
  | [< x = p; l = many p >] -> x :: l

(* p (sep p)* *)
let rec list1 p sep = parser
  | [< x = p; l = list1_aux p sep >] -> x :: l
and list1_aux p sep = parser
  | [< _ = sep; l = list1 p sep >] -> l
  | [<>] -> []

(* (p (sep p)* )? *)
let list0 p sep = parser
  | [< l = list1 p sep >] -> l
  | [<>] -> []

let rec program = parser
  | [< exp = some function_declaration >] -> exp

and function_declaration = parser
  | [< f = function_prototype >] -> f
  | [< f = function_definition >] -> f

and function_prototype = parser
  | [< 'PROTO_KW; t = function_type; 'IDENT n; 'LP; p = list0 function_parameter comma; 'RP; >] -> Function_prototype (t,Identifier (n,Variable) ,p)

and function_definition = parser
  | [< 'FUNC_KW; t = function_type; 'IDENT n; 'LP; p = list0 function_parameter comma; 'RP; i = instruction_parser >] -> Function_definition (t,Identifier (n,Variable),p,i)

and function_parameter = parser
  | [< 'IDENT p; a = function_parameter_array >] -> Parameter (Identifier (p,a))

and function_parameter_array = parser
  | [< 'LB; 'RB>] -> Array (IndexNone)
  | [< >] -> Variable


and function_type = parser
  | [< 'VOID_KW >] -> TypeVoid
  | [< 'INT_KW >] -> TypeInt



and instruction_parser = parser
  | [< i = instruction_block >] -> i
  | [< i = return_instruction >] -> i
  | [< i = print_instruction >] -> i
  | [< i = read_instruction >] -> i
  | [< i = if_instruction >] -> i
  | [< i = while_instruction >] -> i
  | [< 'IDENT base; exp = instruction_litteral base >] -> exp

and instruction_litteral base = parser
  | [< 'LP; p = list0 expression comma; 'RP >] -> FunctionCall (Identifier(base, Variable), p)
  | [< a = array_var; 'ASSIGN; e = expression >] -> Assign ( Identifier(base, a), e)

and instruction_block = parser
  | [< 'LC; declarations = many declaration; instructions = some instruction_parser; 'RC >] ->Block (declarations,instructions)

and declaration = parser
  | [< f = function_declaration >] -> FunDeclar f
  | [< 'INT_KW; v = list1 variable comma >] -> VarDeclar v

and return_instruction = parser
  | [< 'RETURN_KW; exp = expression >] -> Return exp

and print_instruction = parser
  | [< 'PRINT_KW; items = list1 item_parser comma >] -> Print items

and read_instruction = parser
  | [< 'READ_KW; items = list1 item_parser comma >] -> Read items

and item_parser = parser
  | [< exp = expression >] -> ItemExpr exp
  | [< 'TEXT str >] -> ItemString str

and if_instruction = parser
  | [< 'IF_KW; exp = expression; 'THEN_KW; inst_if = instruction_parser; inst_else = else_statement; 'FI_KW>] -> If (exp, inst_if, inst_else)


and else_statement = parser
  | [< 'ELSE_KW; inst = instruction_parser >] -> ElseInstruction inst
  | [< >] -> NoElse

and while_instruction = parser
  | [< 'WHILE_KW; exp = expression; 'DO_KW; inst = instruction_parser; 'DONE_KW >] -> While (exp, inst)




and expression = parser
  | [< e1 = factor; e = expression_aux e1 >] -> e

and expression_aux e1 = parser
  | [< 'PLUS;  e2 = factor; e = expression_aux e2 >] -> ExpBin (e1, PlusOp, e)
  | [< 'MINUS;  e2 = factor; e = expression_aux e2 >] -> ExpBin (e1, MinusOp, e)
  | [<>] -> e1

and factor = parser
  | [< e1 = primary; e = factor_aux e1 >] -> e

and factor_aux e1 = parser
  | [< 'MUL; e = primary; e2 = factor_aux e>] -> ExpBin (e1, MulOp, e2)
  | [< 'DIV; e = primary; e2 = factor_aux e>] -> ExpBin (e1, DivOp, e2)
  | [<>] -> e1

and primary = parser

  | [< 'INTEGER x >] -> ExpInt x
  | [< 'IDENT base; l = litteral base >] -> l
  | [< 'LP; x = expression; 'RP>] -> x

and litteral base = parser
  | [< 'LP; e = list0 expression comma; 'RP >] -> ExpCall (Identifier (base, Variable), e)
  | [< a = array_var >] -> ExpIdent (Identifier (base, a))


and array_var = parser
| [< 'LB; exp = expression; 'RB>] -> Array (IndexExp exp)
| [< 'LB; 'INTEGER size; 'RB>] -> Array (IndexInt size)
| [< >] -> Variable

and variable = parser
  | [< 'IDENT n; a = array_var >] -> Identifier (n,a)



and comma = parser
  | [< 'COM >] -> ()
