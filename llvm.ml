(* TODO : extend when you extend the language *)

(* This file contains a simple LLVM IR representation *)
(* and methods to generate its string representation  *)

open List
open Utils

type llvm_type =
  | LLVM_Type_Int
  | LLVM_Type_Void
  | LLVM_Type_Array of int

(* Warning: because of type inference, we can not
 * have the same field name in two record type
 * (actually this is possible but cumbersome)
 *)

(* fields in each instruction *)
type llvm_binop = {
  lvalue_name: string;
  lvalue_type: llvm_type;
  op: string;
  left: string;
  right: string;
}

and llvm_identifier = {
  value_name : string;
  value_type : llvm_type;
  identifier_name : string;
}

and llvm_assign = {
  value_name : string;
  value_type : llvm_type;
  exp_name : string;
  exp_type : llvm_type;
}

and llvm_return = {
  ret_type: llvm_type;
  ret_value: string; (* we only return identifier, or integers as int *)
}

and llvm_var_declar = {
  declare_type: llvm_type;
  declare_name: string;
}

and llvm_conditional = {
  exp_tmp: string;
  icmp_tmp: string;
  label_then: string;
  label_else: string;
  ir_then: llvm_ir;
  ir_else: llvm_ir;
  label_fi: string;
}

and llvm_while = {
  exp_code: llvm_ir;
  exp_tmp: string;
  icmp_tmp: string;
  label_while: string;
  label_do: string;
  label_done: string;
  ir_do: llvm_ir;
}

and llvm_fun_def = {
  fun_type: llvm_type;
  fun_name: string;
  fun_body: llvm_ir;
  fun_params: string list;
}

and llvm_fun_call = {
  call_name: string;
  call_params: string list;
}

and llvm_exp_call = {
  call_name: string;
  call_params: string list;
  call_place: string;
}

and llvm_print_head = {
  print_head_name: string;
  print_head_content: string;
  print_head_content_len: string;
}

and llvm_print = {
  print_str_ref: string;
  print_exp_list: string list;
  print_content_len: string;
}

and llvm_read = {
  read_str_ref: string;
  read_exp_list: string list;
  read_content_len: string;
}

(* instructions sum type *)
and llvm_instr =
  | Binop of llvm_binop
  | LLVM_Identifier of llvm_identifier
  | LLVM_Return of llvm_return
  | LLVM_Assign of llvm_assign
  | LLVM_Declaration_variable of llvm_var_declar
  | LLVM_Conditional of llvm_conditional
  | LLVM_While of llvm_while
  | LLVM_Fun_def of llvm_fun_def
  | LLVM_Fun_call of llvm_fun_call
  | LLVM_Print_head of llvm_print_head
  | LLVM_Print of llvm_print
  | LLVM_Read of llvm_read
  | LLVM_String of string
  | LLVM_Exp_call of llvm_exp_call


(* Note: instructions in list are taken in reverse order in
 * string_of_ir in order to permit easy list construction !!
 *)
and llvm_ir = {
  header: llvm_instr list; (* to be placed before all code (global definitions) *)
  code: llvm_instr list;
}

and llvm_irr = LLVM_Ir of llvm_ir

(* handy *)
let empty_ir = {
  header = [];
  code = [];
}

(* actual IR generation *)
let rec string_of_llvm_type = function
  | LLVM_Type_Int -> "i32"
  | LLVM_Type_Void -> "void"
  | LLVM_Type_Array size -> "[" ^ string_of_int size ^ " x i32]"

and string_of_ir ir =
 (string_of_instr_list ir.header) ^ (string_of_instr_list ir.code)

and string_of_instr_list l =
  String.concat "" (rev_map string_of_instr l)

and string_of_instr = function
  | Binop v -> "\t" ^ v.lvalue_name ^ " = " ^ v.op ^ " " ^ (string_of_llvm_type v.lvalue_type) ^ " " ^ v.left ^ ", " ^ v.right ^ "\n"
  | LLVM_Return v -> "\t" ^ "ret " ^ (string_of_llvm_type v.ret_type) ^ " " ^ v.ret_value ^ "\n"
  | LLVM_Identifier v -> "\t" ^ v.value_name ^ " = load " ^ (string_of_llvm_type v.value_type) ^ ", " ^ (string_of_llvm_type v.value_type) ^ "* " ^ v.identifier_name ^ "\n"
  | LLVM_Assign v -> "\t" ^ "store " ^ (string_of_llvm_type v.value_type) ^ " " ^ v.exp_name ^ ", " ^ (string_of_llvm_type v.value_type) ^ "* " ^ v.value_name ^ "\n"
  | LLVM_Declaration_variable v -> "\t" ^ v.declare_name ^ " = alloca " ^ (string_of_llvm_type v.declare_type) ^"\n"
  | LLVM_Conditional v -> "\t" ^ v.icmp_tmp ^ " = icmp ne i32 " ^ v.exp_tmp ^ ", 0\n" ^ "\tbr i1 " ^ v.icmp_tmp ^ ", label %" ^ v.label_then ^ ", label %" ^ v.label_else ^ "\n\n" ^ v.label_then ^ ":\n" ^ (string_of_instr_list v.ir_then.code) ^ "\tbr label %" ^ v.label_fi ^ "\n\n" ^ v.label_else ^ ":\n" ^ (string_of_instr_list v.ir_else.code) ^ "\tbr label %" ^ v.label_fi ^ "\n\n" ^ v.label_fi ^ ":\n"

  | LLVM_While v -> "\tbr label %" ^ v.label_while ^ "\n\n" ^ v.label_while ^ ":\n" ^ (string_of_instr_list v.exp_code.code) ^ "\t" ^ v.icmp_tmp ^ " = icmp ne i32 " ^ v.exp_tmp ^ ", 0\n" ^ "\tbr i1 " ^ v.icmp_tmp ^ ", label %" ^ v.label_do ^ ", label %" ^ v.label_done ^ "\n\n" ^ v.label_do ^ ":\n" ^ (string_of_instr_list v.ir_do.code) ^ "\tbr label %" ^ v.label_while ^ "\n" ^ v.label_done ^ ":\n"

  | LLVM_Fun_def v -> (string_of_instr_list v.fun_body.header) ^ "define " ^ (string_of_llvm_type v.fun_type) ^ " @" ^ v.fun_name ^ "(" ^ (param_to_string v.fun_params) ^ ")\n" ^ "{\n" ^ (param_to_alloca v.fun_params) ^ (string_of_instr_list v.fun_body.code) ^ void_ret v.fun_type ^ "}\n"

  | LLVM_Fun_call v -> "\tcall void @" ^ v.call_name ^ "(" ^ (string_list_to_string v.call_params) ^ ")\n"


  | LLVM_Exp_call v -> "\t" ^ v.call_place ^ " = call i32 @" ^ v.call_name ^ "(" ^ (string_list_to_string v.call_params) ^ ")\n"


  | LLVM_Print_head v ->
    v.print_head_name ^ " = global [" ^ v.print_head_content_len ^ " x i8] c\"" ^ v.print_head_content ^ "\"\n"

  | LLVM_Print v ->
    "\tcall i32 (i8*, ...) @printf(i8* getelementptr inbounds ([" ^ v.print_content_len ^ " x i8], [" ^ v.print_content_len ^ " x i8 ]* " ^ v.print_str_ref ^ " , i64 0, i64 0)" ^ (exp_place_to_int v.print_exp_list) ^ ")\n"

    | LLVM_Read v ->
      "\tcall i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([" ^ v.read_content_len ^ " x i8], [" ^ v.read_content_len ^ " x i8 ]* " ^ v.read_str_ref ^ " , i64 0, i64 0)" ^ (exp_place_to_int v.read_exp_list) ^ ")\n"


  | LLVM_String v -> v ^ "\n"

and string_list_to_string lst =
  match lst with
  | [h] -> ("i32 " ^ h)
  | h::t -> ("i32 " ^ h ^", ") ^ (string_list_to_string t)

and void_ret = function
  | LLVM_Type_Void -> "\tret void\n"
  | LLVM_Type_Int -> "\tret i32 0\n"

and param_to_string lst =
  match lst with
  | [] -> ""
  | h::[] -> "i32 %param" ^ h
  | h::t -> "i32 %param" ^ h ^ ", "^ (param_to_string t)

and param_to_alloca lst =
  match lst with
  | h::t -> "\t%" ^ h ^ " = alloca i32\n\tstore i32 %param" ^ h ^ ", i32* %" ^ h ^ "\n" ^ (param_to_alloca t)
  | [] -> ""

and exp_place_to_int lst =
  match lst with
  | h::t -> " , i32 " ^ h ^ (exp_place_to_int t)
  | [] -> ""
